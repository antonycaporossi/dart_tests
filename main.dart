class Person{
	String name, lastName, nationality;
	int age;

	void showName(){
		print(this.name);
	}

	void sayHello(){
		print("Hello");
	}

	void showNationality(){
		print("American");
	}
}

class Bonni extends Person{
	String profession;

	void showProfession() => print(profession);

	@override
	void showNationality(){
		print("Korean/American");
	}
}

class Paulo extends Person{
	
	@override
	void sayHello(){
		print('Olà');
	}

	@override
	void showNationality(){
		print('Portuguese');
	}
}